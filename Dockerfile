# base image
FROM node:12.2.0-alpine
WORKDIR /usr/src/app
copy package.json package-lock.json ./
RUN npm install
COPY . ./
# start app
CMD ["npm", "start"]