import dotenv from 'dotenv';
dotenv.config();

import { createServer, plugins } from 'restify';
import { BadRequestError, NotFoundError } from 'restify-errors';
import corsMiddleware from 'restify-cors-middleware';
import controller from './controller';

const port = process.env.PORT || 4200;

const server = createServer({
  name: 'todo server'
});

const cors = corsMiddleware({
  origins: ['*'],
  allowHeaders: ['X-App-Version'],
  exposeHeaders: []
});

server.use(plugins.bodyParser());

server.pre(cors.preflight);
server.use(cors.actual);

server.pre((req, res, next) => {
  console.info(`${req.method} - ${req.url}`);
  return next();
});

const wrap = (
  fn: (
    req: any,
    res: any,
    next: any
  ) => { catch: (arg0: (err: any) => void) => void }
) => (req: any, res: any, next: (arg0: any) => void) =>
  fn(req, res, next).catch(err => next(err));

server.get(
  '/api/todo',
  wrap(async (req, res, next) => {
    res.send(200, await controller.getAll());
    return next();
  })
);

server.get(
  '/api/todo/:id',
  wrap(async (req, res, next) => {
    if (!req.params.id) {
      return next(new BadRequestError());
    }
    try {
      const product = await controller.getById(+req.params.id);
      res.send(200, product);
      return next();
    } catch (error) {
      return next(new NotFoundError(error));
    }
  })
);

server.post(
  '/api/todo',
  wrap(async (req, res, next) => {
    if (!req.body || !req.body.name || !req.body.id) {
      return next(new BadRequestError());
    }
    controller.create(req.body);
    res.send(201);
    return next();
  })
);

server.put(
  '/api/todo/:id',
  wrap(async (req, res, next) => {
    if (!req.params.id || !req.body || !req.body) {
      return next(new BadRequestError());
    }
    try {
      const todo = await controller.update(+req.params.id, req.body);
      res.send(200, todo);
      return next();
    } catch (error) {
      return next(new NotFoundError(error));
    }
  })
);

server.del(
  '/api/todo/:id',
  wrap(async (req, res, next) => {
    const { id } = req.params;
    if (!id) {
      return next(new BadRequestError());
    }
    try {
      await controller.del(+id);
      res.send(204);
      return next();
    } catch (error) {
      return next(new NotFoundError(error));
    }
  })
);

server.listen(port, () => {
  console.info(`api is running on port ${port}`);
});
