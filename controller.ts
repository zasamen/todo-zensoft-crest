import { MongoClient } from 'mongodb';
const url =
  process.env.DB_USERNAME && process.env.DB_PASSWORD
    ? `mongodb://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process
        .env.DB_URL || 'localhost'}:27017/todo-app`
    : `mongodb://${process.env.DB_URL || 'localhost'}:27017/`;

interface ITodo {
  _id?: number;
  id?: number;
  name: string;
  description: string;
  isDone: boolean;
}

class Todo implements ITodo {
  description: string;
  _id: number;
  id: number;
  name: string;
  isDone: boolean;
  constructor(
    id: number,
    name: string,
    isDone: boolean = false,
    description = ''
  ) {
    this._id = this.id = id;
    this.name = name;
    this.isDone = isDone;
    this.description = description;
  }
}

class TodoController {
  private _client: MongoClient | undefined;

  constructor() {
    MongoClient.connect(url).then(client => (this._client = client));
  }

  async _findTodoById(id: number) {
    if (!this._client) return [];
    let db = this._client.db('todo-app');
    const todo = await db.collection('todo').findOne({ _id: id });
    if (!todo) {
      throw new Error('Todo not found.');
    }
    return todo as Todo;
  }

  async getAll() {
    if (!this._client) return [];
    let db = this._client.db('todo-app');
    const cursor = db.collection('todo').find();
    return await cursor.toArray();
  }

  getById(id: number) {
    return this._findTodoById(id);
  }

  async create(todo: Todo) {
    if (!this._client) return;
    let db = this._client.db('todo-app');
    return await db
      .collection('todo')
      .insertOne({ ...todo, _id: todo.id })
      .catch(err => {
        if (err) throw err;
      });
  }

  async update(id: number, body: Todo) {
    if (!this._client) return;
    let db = this._client.db('todo-app');
    return await db
      .collection('todo')
      .findOneAndUpdate({ _id: id }, { $set: body })
      .catch(err => {
        if (err) throw err;
      });
  }

  async del(id: number) {
    if (!this._client) return;
    let db = this._client.db('todo-app');
    return await db
      .collection('todo')
      .findOneAndDelete({ _id: id })
      .catch(err => {
        if (err) throw err;
      });
  }
}

export default new TodoController();
